#include <iostream>
#include <cmath>

class Vector
{
public:
    Vector(): x(0), y(0), z(0)
    {}
    Vector(double _x, double _y, double _z): x(_x), y(_y), z(_z)
    {}
    void Show()
    {
        std::cout << x << ' ' << y << ' ' << z << '\n';
    }
    double GetLength()
    {
        return std::sqrt(x * x + y * y + z * z);
    }
private:
    double x;
    double y;
    double z;
};

class Circle
{
public:
    Circle(int _x, int _y, int _radius): x(_x), y(_y), radius(_radius)
    {}
    void PrintInfo()
    {
        std::cout << "Circle with center in (" << x << ", " << y << ") with radius " << radius << '\n';
    }
private:
    int x, y, radius;
};

int main()
{
    Circle temp(4, 7, 12);
    temp.PrintInfo();

    Vector v(25, 30, 10);
    v.Show();
    std::cout << "vector length: " << v.GetLength() << "\n";
}
